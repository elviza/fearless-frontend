function createCard(name, description, pictureUrl, starts, ends, subtitle) {
    return `
      <div class="card shadow-sm p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="class-subtitle mb-2 text-muted">${subtitle}</h6>
          <p class="card-text">${description}</p>
          </div>
          <ul class = "list-group list-group-flush">
            <li class="list-group-item">${starts} - ${ends}</li>
          </ul>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let count = 0
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toLocaleDateString();
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const subtitle = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, starts, ends, subtitle);
            const column = document.querySelector(`.col.num${count % 3}`);
            column.innerHTML += html;
            count ++
          }
        }
  
      }
    } catch (e) {
        console.error(e);
    }
  
  });

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         throw new Error('Response not ok')
//       } else {
//             const data = await response.json();
            
//             const conference = data.conferences[0];
//             const nameTag = document.querySelector('.card-title');
//             nameTag.innerHTML = conference.name;
            
//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 const conferenceDetail = details.conference;
//                 const detailTag = document.querySelector('.card-text');
//                 detailTag.innerHTML = conferenceDetail.description;
//                 const imageTag =document.querySelector('.card-img-top');
//                 imageTag.src = details.conference.location.picture_url;
//             }
//       }
//     } catch (e) {
//         // console.error('error', error);
//     }
  
//   });